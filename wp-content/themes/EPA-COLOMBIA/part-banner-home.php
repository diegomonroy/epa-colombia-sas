<!-- Begin Banner -->
	<section class="banner wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'banner_home' ); ?>
			</div>
		</div>
	</section>
<!-- End Banner -->