<!-- Begin Banner -->
	<section class="banner wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'banner_muestreos_directos_en_chimenea' ); ?>
			</div>
		</div>
	</section>
<!-- End Banner -->