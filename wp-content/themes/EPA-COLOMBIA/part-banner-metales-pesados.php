<!-- Begin Banner -->
	<section class="banner wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'banner_metales_pesados' ); ?>
			</div>
		</div>
	</section>
<!-- End Banner -->