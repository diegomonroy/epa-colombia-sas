<!-- Begin Home 2 -->
	<section class="home_2 wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'home_2' ); ?>
			</div>
		</div>
	</section>
<!-- End Home 2 -->