<!-- Begin Top -->
	<section class="top wow fadeIn" data-wow-delay="0.5s">
		<div class="row align-justify align-middle">
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'search' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->
<!-- Begin Menu -->
	<section class="menu wow fadeIn" data-wow-delay="0.5s">
		<div class="row align-center align-middle">
			<div class="small-12 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
		</div>
	</section>
<!-- End Menu -->