<?php
if ( is_front_page() ) :
	get_template_part( 'part', 'banner-home' );
endif;
if ( is_page( 'a-que-nos-dedicamos' ) ) :
	get_template_part( 'part', 'banner-a-que-nos-dedicamos' );
endif;
if ( is_page( 'muestreos-directos-en-chimenea' ) ) :
	get_template_part( 'part', 'banner-muestreos-directos-en-chimenea' );
endif;
if ( is_page( 'material-particulado-o-particulas-suspendidas-totales' ) ) :
	get_template_part( 'part', 'banner-material-particulado-o-particulas-suspendidas-totales' );
endif;
if ( is_page( 'oxidos-de-azufre' ) ) :
	get_template_part( 'part', 'banner-oxidos-de-azufre' );
endif;
if ( is_page( 'oxidos-de-nitrogeno' ) ) :
	get_template_part( 'part', 'banner-oxidos-de-nitrogeno' );
endif;
if ( is_page( 'fluoruros-totales' ) ) :
	get_template_part( 'part', 'banner-fluoruros-totales' );
endif;
if ( is_page( 'haluros-de-hidrogeno-y-halogenos' ) ) :
	get_template_part( 'part', 'banner-haluros-de-hidrogeno-y-halogenos' );
endif;
if ( is_page( 'dioxinas-y-furanos' ) ) :
	get_template_part( 'part', 'banner-dioxinas-y-furanos' );
endif;
if ( is_page( 'metales-pesados' ) ) :
	get_template_part( 'part', 'banner-metales-pesados' );
endif;
if ( is_page( 'compuestos-organicos-totales' ) ) :
	get_template_part( 'part', 'banner-compuestos-organicos-totales' );
endif;
if ( is_page( 'monitoreo-de-calidad-del-aire' ) ) :
	get_template_part( 'part', 'banner-monitoreo-de-calidad-del-aire' );
endif;
if ( is_page( 'modelos-de-dispersion-atmosferica' ) ) :
	get_template_part( 'part', 'banner-modelos-de-dispersion-atmosferica' );
endif;
?>
<!-- Begin Content -->
	<section class="content wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->