<?php

/*

Functions for my template

*/

/*
 * Function to add my styles files
 */
function my_styles_files() {
	wp_enqueue_style( 'foundation-flex-css', get_template_directory_uri() . '/build/bower_components/foundation-sites/dist/css/foundation-flex.min.css', false );
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/build/bower_components/animate.css/animate.min.css', false );
	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/build/bower_components/fancybox/dist/jquery.fancybox.min.css', false );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/build/bower_components/font-awesome/css/font-awesome.min.css', false );
	if ( is_child_theme() ) {
		wp_enqueue_style( 'parent-css', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
	}
	wp_enqueue_style( 'theme-css', get_stylesheet_uri(), false );
}
add_action( 'wp_enqueue_scripts', 'my_styles_files' );

/*
 * Function to add my scripts files
 */
function my_scripts_files() {
	//wp_deregister_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_files' );

/*
 * Function to add my scripts files in footer
 */
function my_scripts_files_footer() {
	//wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/build/bower_components/jquery/dist/jquery.min.js', false );
	wp_enqueue_script( 'what-input-js', get_template_directory_uri() . '/build/bower_components/what-input/dist/what-input.min.js', false );
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/build/bower_components/foundation-sites/dist/js/foundation.min.js', false );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/build/bower_components/fancybox/dist/jquery.fancybox.min.js', false );
	wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/build/bower_components/wow/dist/wow.min.js', false );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/build/app.js', false );
}
add_action( 'wp_footer', 'my_scripts_files_footer' );

/*
 * Function to register my menus
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'main-menu' => __( 'Main Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/*
 * Function to register my sidebars and widgetized areas
 */
function arphabet_widgets_init() {
	register_sidebar(
		array(
			'name' => 'Logo',
			'id' => 'logo',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search',
			'id' => 'search',
			'before_widget' => '<div class="moduletable_to2 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Home',
			'id' => 'banner_home',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner A Que Nos Dedicamos',
			'id' => 'banner_a_que_nos_dedicamos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Muestreos Directos En Chimenea',
			'id' => 'banner_muestreos_directos_en_chimenea',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner PST',
			'id' => 'banner_pst',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Óxidos de Azufre',
			'id' => 'banner_oxidos_de_azufre',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Óxidos de Nitrógeno',
			'id' => 'banner_oxidos_de_nitrogeno',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Fluoruros Totales',
			'id' => 'banner_fluoruros_totales',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Haluros de Hidrogeno y Halógenos',
			'id' => 'banner_haluros_de_hidrogeno_y_halogenos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Dioxinas y Furanos',
			'id' => 'banner_dioxinas_y_furanos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Metales Pesados',
			'id' => 'banner_metales_pesados',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Compuestos Orgánicos Totales',
			'id' => 'banner_compuestos_organicos_totales',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Monitoreo de Calidad del Aire',
			'id' => 'banner_monitoreo_de_calidad_del_aire',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Modelos de Dispersión Atmosférica',
			'id' => 'banner_modelos_de_dispersion_atmosferica',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Home 1',
			'id' => 'home_1',
			'before_widget' => '<div class="moduletable_h11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Home 2',
			'id' => 'home_2',
			'before_widget' => '<div class="moduletable_h21">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="text-center">',
			'after_title' => '</h3>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Home 3',
			'id' => 'home_3',
			'before_widget' => '<div class="moduletable_h31">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Bottom',
			'id' => 'bottom',
			'before_widget' => '<div class="moduletable_bo1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
}
add_action( 'widgets_init', 'arphabet_widgets_init' );